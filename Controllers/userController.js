const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const User = require("../Models/usersSchema.js");
const auth = require("../auth.js");

// User registration
module.exports.userRegistration = (request, response) => {
	let input = request.body;
	User.findOne({email: input.email})
	.then(result => {
		if(result !== null){
			return response.send(false);
		}
		else{
			let newUser = new User({
				username: input.username,
				email: input.email,
				password: bcrypt.hashSync(input.password, 10)
			})
			newUser.save()
			.then(save => response.send(true))
			.catch(error => response.send(false))
		}
	})
	.catch(error => response.send(error))
};

// User authentication (Login)
module.exports.userAuthentication = (request, response) => {
	let input = request.body;
	User.findOne({email: input.email})
	.then(result => {
		if(result === null){
			return response.send(false)
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)
			if(isPasswordCorrect){
				return response.send({auth: auth.createAccessToken(result)});
			}
			else{
				return response.send(false);
			}
		}
	})
	.catch(error => response.send(error));
};

// User details
module.exports.userDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	User.findById(userData._id)
	.then(result => {
		result.password = "hidden";
		return response.send(result);
	})
};