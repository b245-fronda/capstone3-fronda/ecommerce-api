const mongoose = require("mongoose");
const Product = require("../Models/productsSchema.js");
const Order = require("../Models/ordersSchema.js");
const auth = require("../auth.js");

// Non-admin user checkout (Create order)
module.exports.createOrder = (request, response) => {
	let input = request.body;
	const productId = request.params.productId;
	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin === true){
		return response.send(false)
	}
	else{
		Product.findById(productId)
		.then(product => {
			let newOrder = new Order({
				userId: userData._id,
				username: input.username,
				productId: productId,
				image: input.image,
				name: input.name,
				description: input.description,
				price: input.price,
				quantity: input.quantity,
				totalAmount: input.quantity * product.price,
			})
			newOrder.save()
			.then(order => response.send(true))
			.catch(error => response.send(false))
		})
		.catch(error => response.send(false))
	}
};

// Retrieve all orders (Admin only)
module.exports.allOrders = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin === false){
		return response.send(false)
	}
	else{
		Order.find({})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
};

// Retrieve authenticated user's orders
module.exports.userOrders = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const userId = request.params.userId;
	Order.find({userId: userId})
	.then(result => {
		if(userId === userData._id){
			if(result === null){
				return response.send("You have no orders yet.")
			}
			else{
				return response.send(result)
			}
		}
		else if(userData.isAdmin === true){
			if(result === null){
				return response.send("User has no orders yet.")
			}
			else{
				return response.send(result)
			}
		}
		else{
			return response.send("Acess denied")
		}
	})
	.catch(error => response.send(error))
};