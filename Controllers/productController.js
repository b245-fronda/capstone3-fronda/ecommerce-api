const mongoose = require("mongoose");
const Product = require("../Models/productsSchema.js");
const auth = require("../auth.js");

// Create product (Admin only)
module.exports.addProduct = (request, response) => {
	let input = request.body;
	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin === false){
		return response.send("Access denied. Not an admin.")
	}
	else{
		Product.findOne({name: input.name})
		.then(result => {
			if(result !== null){
				return response.send(false)
			}
			else{
				let newProduct = new Product({
					image: input.image,
					name: input.name,
					description: input.description,
					price: input.price
				})

				newProduct.save()
				.then(save => response.send(true))
				.catch(error => response.send(false))
			}
		})
		.catch(error => response.send(false))
	}
};

// Retrieve all products (Admin only)
module.exports.allProducts = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin === false){
		return response.send(false)
	}
	else{
		Product.find({})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
};

// Retrieve all active products
module.exports.allActiveProducts = (request, response) => {
	Product.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error))
};

// Retrieve single product
module.exports.productDetails = (request, response) => {
	const productId = request.params.productId;
	Product.findById(productId)
	.then(result => response.send(result))
	.catch(error => response.send(error))
};

// Update product information (Admin only)
module.exports.updateProduct = (request, response) => {
	let input = request.body;
	const productId = request.params.productId;
	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin === false){
		return response.send(false)
	}
	else{
		Product.findById(productId)
		.then(result => {
			if(result === null){
				return response.send(false)
			}
			else{
				let updatedProduct = {
					image: input.image,
					name: input.name,
					description: input.description,
					price: input.price
				}
				Product.findByIdAndUpdate(productId, updatedProduct, {new: true})
				.then(result => response.send(true))
				.catch(error => response.send(false))
			}
		})
		.catch(error => response.send(false))
	}
};

// Archive product (Admin only)
module.exports.archiveProduct = (request, response) => {
	let input = request.body;
	const productId = request.params.productId;
	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin === false){
		return response.send(false)
	}
	else{
		Product.findById(productId)
		.then(result => {
			if(productId === null){
				return response.send(false)
			}
			else{
				let archivedProduct = {
					isActive: input.isActive
				}
				Product.findByIdAndUpdate(productId, archivedProduct, {new: true})
				.then(result => response.send(true))
				.catch(error => response.send(false))
			}
		})
		.catch(error => response.send(error))
	}
};