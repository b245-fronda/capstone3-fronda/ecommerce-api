const mongoose = require("mongoose");

const cartsSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User Id is required"]
	},
	username: {
		type: String,
		required: [true, "Username is required"]
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, "Product Id is required"]
			},
			images: [
				{
					image: {
						type: String,
						required: [true, "Product image is required"]
					}
				}
			],
			name: {
				type: String,
				required: [true, "Product name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "quantity is required"]
			},
			price: {
				type: Number,
				required: [true, "Price is required"]
			}
		}
	],
	totalAmount: {
		type: Number,
		required: [true, "Total Amount is required"]
	}
});

module.exports = mongoose.model("Cart", cartsSchema);