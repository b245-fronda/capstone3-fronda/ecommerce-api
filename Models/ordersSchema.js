const mongoose = require("mongoose");

const ordersSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User Id is required"]
	},
	username: {
		type: String,
		required: [true, "Username is required"]
	},
	productId: {
		type: String,
		required: [true, "Product Id is required"]
	},
	image: {
		type: String,
		required: [true, "Product image is required"]
	},
	name: {
		type: String,
		required: [true, "Product name is required"]
	},
	description: {
		type: String,
		required: [true, "Product description is required"]
	},
	quantity: {
		type: Number,
		required: [true, "Quantity is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	totalAmount: {
		type: Number,
		required: [true, "Total Amount is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Order", ordersSchema);